#include <bits/stdc++.h>

using namespace std;

int PORT = 5006;
int BUFFER_SIZE = 1e3;
string IP_ADDRESS = "127.0.0.1";

// general string utilities trim, reduce are copied from 
// https://stackoverflow.com/questions/1798112/removing-leading-and-trailing-spaces-from-a-string

string trim(const string& str,
                 const string& whitespace = " \t")
{
    const auto strBegin = str.find_first_not_of(whitespace);
    if (strBegin == string::npos)
        return ""; // no content

    const auto strEnd = str.find_last_not_of(whitespace);
    const auto strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}

string reduce(const string& str,
                   const string& fill = " ",
                   const string& whitespace = " \t")
{
    // trim first
    auto result = trim(str, whitespace);

    // replace sub ranges
    auto beginSpace = result.find_first_of(whitespace);
    while (beginSpace != string::npos)
    {
        const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
        const auto range = endSpace - beginSpace;

        result.replace(beginSpace, range, fill);

        const auto newStart = beginSpace + fill.length();
        beginSpace = result.find_first_of(whitespace, newStart);
    }

    return result;
}

vector<string> split_str(string s, char delim){ // split string
    vector <string> tokens; 
    stringstream check1(s); 
    string intermediate; 
    while(getline(check1, intermediate, delim)) 
        tokens.push_back(intermediate); 
    return tokens;
}

int get_int(string s) {
    int ans = 0;
    int base = 0;
    reverse(s.begin(),s.end());
    for (auto i : s){
        int x = '9'-i;
        if (x>9 || x<0) continue;
        ans += x*(pow(10,base));
        base+=1;
        cout << i  << " " << x << endl;
    }
    reverse(s.begin(), s.end());
    return ans;
}

string serialize_answer(int answer, string explaination) {
    return to_string(answer) +"|"+explaination;
}

string serialize_answer(pair<int, string> answer) {
    return to_string(answer.first) + "|" + answer.second;
}

void deserialize_answer(string s, int *answer, string *explaination) {
    auto splits = split_str(s,'|');
    *answer = stoi(splits[0]);
    *explaination = splits[1];
}

class Question {
    private:
    int id;
    string question;
    vector<string> options;
    int answer;
    string explaination;
    public:
    Question(int id, string question, vector<string> options){
        this->id = id;
        this->question = question;
        this->options = options;
    }
    Question(int id, string question, vector<string> options, int answer, string explaination){
        this->id = id;
        this->question = question;
        this->options = options;
        this->answer = answer;
        this->explaination = explaination;
    }
    int get_id() {return id;}
    string serialize_question(){
        string s =  to_string(id)+"|"+question+"|";
        for (auto i :options)
            s += i+"-";
        return s;
    }
    string get_option(int index) {return options[index];}
    string ask() {
        cout << "#> " << question << endl;
        for (int i = 0; i < options.size(); i++)
            printf("  %c : %s\n", 'A'+i, options[i].c_str());
        string s;
        cout << "#> ";
        cin >> s;
        return s;
    }
    string serialize_answer() {
        return to_string(answer) +"|"+explaination;
    }
};

Question deserialize_question(string s) {
    //cout << s << endl;
    vector<string> splits = split_str(s, '|');
    //for (auto i : splits) cout << i << " ";
    //cout << trim(splits[0]).length() << endl;
    //for (auto i : trim(splits[0])) cout << i << " ";
    //cout << endl;
    //cout<< stoi(trim(splits[0]), nullptr, 10) << endl;
    //if (trim(splits[0]) =="1") cout << "yes" << endl;
    //cout << get_int(splits[0]) << endl;
    //int id = stoi(splits[0]);
    string question = splits[1];
    vector<string> options = split_str(splits[2], '-');
    return Question(0, question, options);
}