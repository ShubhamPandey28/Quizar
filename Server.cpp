#include <bits/stdc++.h>
#include <unistd.h>
#include <cstring>      // Needed for memset
#include <sys/socket.h> // Needed for the socket functions
#include <netdb.h>      // Needed for the socket functions
#include <netinet/in.h> // Needed for internet addresses
#include "common_utils.hpp"
using namespace std;

class ServerSocket {
    private:
    int socket_fd, port;
    struct sockaddr_in server_address, client_address;
    socklen_t client_address_length;
    set<int> clients; // <client_socket_id , buffers>
    int buffer_size;

    public:
    ServerSocket(int port, int buffer_size){
        this->port = port;
        this->buffer_size = buffer_size;
        bzero(&server_address, sizeof(server_address));

        server_address.sin_family = AF_INET;
        server_address.sin_addr.s_addr = htonl(INADDR_ANY);
        server_address.sin_port = htons(port);

        socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    }

    int _listen(int max_connections){
        int bind_result = bind(socket_fd, 
                                (struct sockaddr *)&server_address,
                                sizeof(server_address));
        int listen_result = listen(socket_fd, max_connections);
        if (bind_result || listen_result) return -1;
        return listen_result;
    }
    int _accept() {
        client_address_length = sizeof(client_address_length);
        int client_socket_fd = accept(socket_fd, (struct sockaddr *)&client_address, 
                                        &client_address_length);
        return client_socket_fd;
    }

    string get_message(int client_socket_fd) {
        string header(16, 0);
        int read_result = read(client_socket_fd, &header[0], 15);
        if (read_result > 0) {
            cout << header << endl;
            int size = stoi(header)+1;
            string message(size, 0);
            cout << message.length() << endl;
            read_result = read(client_socket_fd, &message[0], size*sizeof(char));
            if (read_result > 0) return message;
        }
        cerr << "Unable to recieve message from client socket " << client_socket_fd << endl;
        return "";
    }
    int _send(int client_socket_fd, string message) {
        string header = to_string(message.length());
        header.resize(16);
        cout << header << endl;
        int write_result = write(client_socket_fd, header.c_str(), header.length());
        if (write_result > 0) {
            write_result = write(client_socket_fd, message.c_str(), message.length());
            cout << "write_result " << write_result<< endl;
        }
        if (write_result <=0) cerr << "Unable to send message : "<< message  << endl;
        return write_result;
    }

    void remove_client(int client_socket_id)  { clients.erase(client_socket_id); }
    void _close() {
        close(socket_fd);
    }

    ~ServerSocket(){
        _close();
    }
};

int NEW_QUES_ID = 0;
map<int , pair<int, string>> ANSWER;
vector<Question> read_questions(string filename){
    vector<Question> questions;
    ifstream quesfile(filename);
    string question, option, ans, explaination;
    while (getline(quesfile, question)){
        question = question.substr(9);
        getline(quesfile, option);
        option = option.substr(8);
        getline(quesfile, ans);
        ans = ans.substr(7);
        getline(quesfile, explaination);
        explaination = explaination.substr(13);
        // questions.push_back(Question(NEW_QUES_ID, question, split_str(option,'~'));
        questions.push_back(Question(NEW_QUES_ID, question, split_str(option,'~'), stoi(ans), explaination));
        ANSWER[NEW_QUES_ID] = {stoi(ans), explaination};
        NEW_QUES_ID++;
    }
    quesfile.close();
    return questions;
}

int main()
{
    map<string, vector<Question>> questions;
    questions["T"] = read_questions("Questions/threads.txt");
    questions["S"] = read_questions("Questions/scheduling.txt");
    questions["M"] = read_questions("Questions/memory.txt");
    ServerSocket server_socket = ServerSocket(5005, 1e3);
    if (server_socket._listen(5) == -1) {
        cout << "Unable to listen";
        return 1;
    }
    cout << "Listening" << endl;
    pid_t pid;
    while (1) {
        int client_socket_fd = server_socket._accept();
        if (client_socket_fd == -1) continue;
        pid = fork();
        if (pid == 0) {
            cout << "Connection on socket fd: " << client_socket_fd << endl;;
            string message = server_socket.get_message(client_socket_fd);
            vector<string> splits = split_str(message,'|');
            string mode = splits[0];
            string topic = splits[1];
            vector<int> randArr;
            for(int i = 0; i<questions[topic].size();i++)
                randArr.push_back(i);
            random_shuffle(randArr.begin(),randArr.end());
            for (int j : randArr) {
                Question i = questions[topic][j];
                server_socket._send(client_socket_fd, i.serialize_question());
                string answered = server_socket.get_message(client_socket_fd);
                cout << "answered " << answered << endl;
                server_socket._send(client_socket_fd, i.serialize_answer());
            }
            server_socket._send(client_socket_fd,"#");
        }
    }
}