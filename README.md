# Quizar
An online quizzing platform on OS as an assignment for course CS310(Introduction to distributed communicating processes).

## Question file syntax

```
question:<question string>
options:<option 1 string>~<option 2 string>~...~<option n string>
answer:<index in options>
explaination:<explaination string>
```
#### NOTE : DO NOT GIVE NEWLINE INSIDE ANY PARAMETER ABOVE

## Running steps

1. ```make``` for compilation
2. ```./server``` for running server
3. ```./client``` for running client

### Video [link](https://drive.google.com/file/d/1BKTCDLIdaMAtUGHNDYZnl_qWluorE3L0/view?usp=sharing)
