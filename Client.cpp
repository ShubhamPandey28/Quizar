#include <bits/stdc++.h>
#include <unistd.h>
#include <sys/socket.h> // Needed for the socket functions
#include <netdb.h>      // Needed for the socket functions
#include <netinet/in.h>
#include <arpa/inet.h>
#include "common_utils.hpp"

using namespace std;

class ClientSocket {
    private:
    struct sockaddr_in client_address, server_address;
    int socket_fd;
    socklen_t client_address_length, server_address_length;
    int buffer_size;

    public:
    ClientSocket(string ip_address, int port, int buffer_size){
        this->buffer_size = buffer_size;
        server_address.sin_family = AF_INET;
        server_address.sin_port = htons(port);
        if (inet_pton(AF_INET, ip_address.c_str(), &server_address.sin_addr) <= 0)
            cerr << "Cannot connnect : Invalid IP Address\n";
        socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    }
    int _connect(){
        int connect_result = connect(socket_fd, (struct sockaddr *)&server_address,
                                     sizeof(server_address));
        return connect_result;
    }

    int get_socket_fd() {return socket_fd;}

    int _send(string message) {
        string header = to_string(message.length());
        header.resize(16);
        int write_result = write(socket_fd, header.c_str(), header.length());
        if (write_result > 0) {
            write_result = write(socket_fd, message.c_str(), message.length());
        }
        if (write_result <=0) cerr << "Unable to send message : "<< message  << endl;
        return write_result;
    }

    string _recieve(){
        string header(16, 0);
        int read_result = read(socket_fd, &header[0], 15);
        if (read_result > 0) {
            int size = stoi(header)+1;
            string message(size, 0);
            read_result = read(socket_fd, &message[0], size*sizeof(char));
            if (read_result > 0) return message;
            //else if ( read_result == 0) return _recieve();
        }
        cerr << "Unable to recieve message from server " << endl;
        return "";
    }
    void _close(){
        close(socket_fd);
    }
    ~ClientSocket(){
        close(socket_fd);
    }
};


string cli_input(){
    string var;
    cout << "#> ";
    cin >> var;
    if (var == "q") exit(0);
    else if (var == "clear") {
        system("clear");
        return cli_input();
    }
    return var;
}
string user_id;
string make_header(string type, string data) { // type = "get_ques:T" , "post_ans"
    string header = user_id + "|" + type +"|"+ data;
    return header;
}

int main()
{
    ClientSocket client_socket = ClientSocket("127.0.0.1", 5005, 1e3);
    if (client_socket._connect() < 0) {
        cout << "Unable to connect";
        return 1;
    } 
    cout << "Welcome to 'Quizar' an online quizing platform on OS.\nPress 'q' to exit" << endl;
    cout << "Select mode : \n - press I for individual mode\n - press G for Group mode\n - press A for Admin mode." << endl;
    string mode = cli_input();
    cout << "Select topic :\n - press T for threading\n - press S for scheduling\n - press M for memory management\n";
    string topic = cli_input();
    client_socket._send(mode+"|"+topic);
    string message = "--";
    while (true) {
        string message = client_socket._recieve();
        //cout << convert2char(message) << endl;
        if (message[0] == '#' || message[1]=='#') break;
        //cout << 103 << endl;
        //cout << message << endl;
        //cout << 106 << endl;
        Question ques = deserialize_question(message);
        string answered = ques.ask();
        client_socket._send(answered);
        message = client_socket._recieve();
        //cout << message << endl;
        //int ans;
        //string explaination;
        //deserialize_answer(message, &ans, &explaination);
        //cout << ans << " " << explaination << endl;
        auto ans_exp = split_str(message, '|');
        if (answered[0]-'A' == ans_exp[0][1]-'0') cout << "Right\n";
        else cout << "Wrong\n";
        cout << "Correct Answer : " << ques.get_option(ans_exp[0][1]-'0') << endl;
        cout << "Explaination : " << ans_exp[1] << endl;
    }
}